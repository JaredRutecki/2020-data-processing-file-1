### Forest View

library(tidyverse)
library(readxl)

forest_view <- read_excel("original/Forest View/cleaning_workbook.xlsx",
                          sheet = 1)
forest_view_employees <- read_excel("original/Forest View/cleaning_workbook.xlsx",
                                    sheet = 2)


forest_view %>%
  group_by(pay_code) %>% summarize()

forest_view %>%
  mutate(bga_pay_type = case_when(str_detect(pay_code, "REG") ~ "base_salary",
                                  TRUE ~ "extra_pay")) %>%
  pivot_wider(id_cols = c("employee_name"),
              names_from = bga_pay_type,
              values_from = total,
              values_fn = sum) %>%
  left_join(forest_view_employees) %>% 
  mutate(employer = "Forest View") %>%
  select(employer, employee_name, department, title, base_salary, extra_pay, date_started) %>%
  separate(employee_name, into = c("last_name","first_name"), sep =",") %>%
  mutate(date_started = ymd(date_started),
         extra_pay = replace_na(extra_pay, 0)) %>%
  write_csv("output/forest_view.csv", na = "")
