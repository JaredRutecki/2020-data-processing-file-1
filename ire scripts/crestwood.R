### Crestwood

### I manually deleted department totals as well as extraneous pay columns
library(tidyverse)
library(readxl)
library(janitor)

crestwood <- read_excel("original/Crestwood/00328495.xlsx",
                        skip =2) %>%
  clean_names() %>%
  select(-reg_ytd, -ot_ytd, -non_cash_pay) %>%
  filter(if_any(1:3, ~ !is.na(.))) %>%
  mutate(totals = case_when(str_detect(payment_code_id,"Totals") ~ payment_code_id)) %>%
  fill(totals, .direction = "up") %>%
  filter(totals == "Employee Totals:" | is.na(totals)) %>%
  select(-totals) %>%
  mutate(department = case_when(str_detect(employee_id, "Department") ~ employee_id)) %>%
  separate(department, into = c("drop","dept_no","department"), sep = ":") %>%
  select(-drop) %>%
  mutate(dept_no = str_squish(dept_no),
         department = str_squish(department)) %>%
  fill(c("dept_no","department"), .direction = "down") %>%
  mutate(department = case_when(str_detect(dept_no,"710010") ~ "",
                                !str_detect(dept_no,"710010") ~ department)) %>%
  select(-dept_no) %>%
  filter(!str_detect(employee_id, "Department") | is.na(employee_id)) %>%
  filter(!str_detect(payment_code_id, "Employee Total") | is.na(payment_code_id)) %>%
  mutate(employee_name = case_when(str_detect(employee_id, ",") ~ employee_id)) %>%
  fill(employee_name, .direction = "down") %>%
  select(-employee_id) %>%
  separate(employee_name, into = c("employee_id", "employee_name"), sep = " - ") %>%
  filter(!is.na(payment_code_id)) %>%
  filter(total_ytd > 0.000)


crestwood %>%
  group_by(payment_code_id) %>%
  summarize()

crestwood_extra <- "AUTO|COMP|FIRE OT|OT|SICK|VAC|PERS"

crestwood %>%
  mutate(bga_pay_type = case_when(str_detect(payment_code_id, crestwood_extra) ~ "Extra Pay",
                                  !str_detect(payment_code_id, crestwood_extra) ~ "Base Salary")) %>%
  select(employee_name, department, bga_pay_type, total_ytd) %>%
  pivot_wider(id_cols = c("employee_name", "department"),
              names_from = bga_pay_type,
              values_from = total_ytd,
              values_fn = sum) %>%
  separate(employee_name, into = c("last_name","first_name"), sep = ", ") %>%
  mutate(employer = "Crestwood",
         title = NA,
         date_started = NA) %>%
  rename(extra_pay = 4,
         base_salary = 5) %>%
  select(employer, last_name, first_name, department, title, base_salary, extra_pay, date_started) %>%
  write_csv("output/crestwood.csv", na = "")
