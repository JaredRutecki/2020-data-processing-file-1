### Morton Grove
library(readxl)
library(tidyverse)
library(janitor)
library(fuzzyjoin)

mg_disclosure <- read_excel("original/Morton Grove/2020 Compensation Disclosure.xlsx",
                            col_names = F) %>%
  select(1:4)

mg_register <- read_excel("original/Morton Grove/Payroll Register Data Export YTD 2020.xls",
                          skip = 3,
                          col_names = F) %>%
  clean_names() %>%
  remove_empty(c("cols","rows"))

### That didn't work, so I sorted and cleaned columns manually. Probably a very graceful way to handled those nested columns.

mg_cleaner <- read_csv("original/Morton Grove/mg_pay_cleaning.csv") %>%
  clean_names() %>%
  select(-x3, -x4, -co)

mg_cleaner %>%
  fill(c("department","employee","id"), .direction = "down") %>%
  filter(process != "Total" | is.na(process)) %>%
  select(employee, department, base_salary, extra_pay) %>%
  group_by(employee, department) %>%
  summarize(total_base = sum(base_salary),
            total_extra = sum(extra_pay)) -> mg_salaries

mg_salaries %>%
  arrange(department, employee)

mg_disclosure %>%
  clean_names() %>%
  mutate(employee = paste(x2, x3, sep = ", ")) -> mg_employee_info

mg_employee_info %>% write_csv("original/Morton Grove/employee_standard.csv", na = "")

mg_employee_standardized <- read_csv("original/Morton Grove/employee_standard.csv")

mg_salaries %>%
  regex_left_join(mg_employee_standardized) %>%
  select(employee.x, department, x4, total_base, total_extra) %>%
  mutate(employer = "Morton Grove",
         date_started = NA) %>%
  rename(title = x4,
         base_salary = total_base,
         extra_pay = total_extra) %>%
  select(employer, employee.x, department, title, base_salary, extra_pay, date_started) %>%
  mutate(extra_pay = replace_na(extra_pay, 0)) %>%
  separate(employee.x, into = c("last_name","first_name"), sep = ", ") %>%
  write_csv("output/morton_grove.csv", na = "")
         