### Geneva Park District

library(tidyverse)

geneva_hours <- read_fwf("original/Geneva Park District/Employee YTD Hrs Report 2021.txt",
                         fwf_empty("original/Geneva Park District/Employee YTD Hrs Report 2021.txt"), skip = 11)

geneva_hours <- read_fwf("original/Geneva Park District/gpd_hours_edit.txt",
                         fwf_cols(emp_no = c(1,4),
                                  employee_name = c(12,49),
                                  pay_code = c(50,52),
                                  pay_type = c(53,84),
                                  hours_worked = c(87,98),
                                  amount_paid = c(99,108),
                                  hours_worked_ytd = c(109,121),
                                  amount_paid_ytd = c(122,133))) %>%
  filter(hours_worked != "------------" | is.na(hours_worked)) %>%
  filter(hours_worked != "WORKED" | is.na(hours_worked)) %>%
  filter(hours_worked != "HOURS" | is.na(hours_worked)) %>%
  filter(emp_no != "DATE" | is.na(emp_no)) %>%
  filter(emp_no != "TIME" | is.na(emp_no)) %>%
  filter(emp_no != "ID:" | is.na(emp_no)) %>%
  filter(pay_type != "ORDERED BY EMPLOYEE" | is.na(pay_type)) %>%
  filter(hours_worked_ytd != "---YEAR" | is.na(hours_worked_ytd)) %>%
  filter(hours_worked_ytd != "CURRE" | is.na(hours_worked_ytd)) %>%
  filter(pay_type != "ORDERED BY EMPLOYEE NAME" | is.na(pay_type)) %>%
  filter(pay_type != "TOTAL:" | is.na(pay_type)) %>%
  fill(emp_no, .direction = "down") %>%
  fill(employee_name, .direction = "down") %>%
  select(-hours_worked, -hours_worked_ytd) %>%
  mutate(amount_paid = parse_number(amount_paid),
         amount_paid_ytd = parse_number(amount_paid_ytd)) %>%
  mutate(pay = pmax(amount_paid, amount_paid_ytd)) %>%
  select(-amount_paid, -amount_paid_ytd) %>%
  arrange(employee_name, desc(pay)) %>%
  group_by(employee_name) %>%
  mutate(maximum_pay = max(pay)) %>%
  mutate(job_title = case_when(pay == maximum_pay ~ pay_type)) %>%
  ungroup() %>%
  select(-pay_code, -maximum_pay)

geneva_hours %>%
  group_by(pay_type) %>%
  summarize(count = n()) %>% View()

extra_pay <- "OVERTIME|VACATION|SICK|HOLIDAY HOURS"

geneva_hours %>%
  mutate(bga_pay_type = case_when(str_detect(pay_type, extra_pay) ~ "Extra Pay",
                                  !str_detect(pay_type, extra_pay) ~ "Base Salary")) %>%
  fill(job_title, .direction = "down") %>%
  select(emp_no, employee_name, job_title, pay, bga_pay_type) %>%
  pivot_wider(id_cols = c("emp_no","employee_name","job_title"),
              names_from = bga_pay_type,
              values_from = pay,
              values_fn = sum) %>%
  select(-emp_no) %>%
  separate(employee_name, into = c("last_name","first_name"), sep = ", ") %>%
  mutate(employer = "Geneva Park District",
         department = NA,
         date_started = NA) %>%
  rename(title = job_title,
         base_salary = `Base Salary`,
         extra_pay = `Extra Pay`) %>%
  select(employer, last_name, first_name, department, title, base_salary, extra_pay, date_started) %>%
  write_csv("output/geneva_park_district.csv", na = "")

         